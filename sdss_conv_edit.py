#!/usr/bin/env python
# coding: utf-8

# In[2]:
# started as jupyter notebook but now is standalone python script
# that can take optional flag parameters 

'''
vae has modular desing
endoder, decoder and vae are 3 models that share weights
vae latent vecotr samples from a Gaussian distribution with mean = 0 and std = 1
inspiration from https://github.com/keras-team/keras/blob/master/examples/variational_autoencoder.py
'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from keras.layers import Lambda, Input, Dense
from keras.models import Model
from keras.losses import mse, binary_crossentropy
from keras.utils import plot_model
from keras import backend as K

from keras.callbacks import TensorBoard

import numpy as np
import matplotlib.pyplot as plt
import argparse
import os

import h5py
import random


####BEGIN IMPORTING GAL<STAR<QSO DATA 
#load data from data_path
#our data here is normalized 28x28 images of Stars, QSOs and Galaxies
data_path='./dump/reform.hdf5'
#get total amount of images in count
count=0
#image shapes
data_shape=(0,0)
with h5py.File(data_path, 'r') as f:
    for group in f:
        for data in f.get(group).__iter__():
            #if int(data)>maxval: maxval=int(data)
            #if np.mean(f.get(group).get(data))<cutoff:
            if count<1:
                data_shape=f.get(group).get(data).shape
            count+=1
#save width and height of read-in images to use later
width=data_shape[0]
height=data_shape[1]
original_dim=width*height
#format to keep iamge types and data accounted for together: array of dic(type,data)
#we will use this to make our train and test data
images_temp=[dict() for x in range(count)]
#fill images_temp with data and data_types in each dict entry
index=0
with h5py.File(data_path, 'r') as f:
    for object_type in f:
        for data in f.get(object_type):
            #if np.mean(f.get(group).get(data))<cutoff:
            dat=f.get(object_type).get(data)
            #train_images[index]=dat
            if object_type=='GALAXY':o_type=0
            if object_type=='QSO':o_type=1
            if object_type=='STAR':o_type=2
            images_temp[index]={'type':o_type,'data':np.array(dat).flatten()}
            index+=1
assert(index==count)
#now shuffle dict array to enhance nn training
random.shuffle(images_temp)
#decide size of our test data, just taking test_size last images of images_temp
#set x_train,y_train,x_test,y_test
#x is image data, y is data type
test_size=4436
upper=count
lower=count-test_size
x_train=np.empty([lower,original_dim])
y_train=np.empty(lower)
for i in range(lower):
    x_train[i]=images_temp[i]['data']
    y_train[i]=images_temp[i]['type']
x_test=np.empty([test_size,original_dim])
y_test=np.empty(test_size)
for i in range(lower,count):
    x_test[lower-i]=images_temp[i]['data']
    y_test[lower-i]=images_temp[i]['type']
#now we can run nn (:

# reparameterization trick
# instead of sampling from Q(z|X), sample epsilon = N(0,I)
# z = z_mean + sqrt(var) * epsilon
def sampling(args):

    z_mean, z_log_var = args
    batch = K.shape(z_mean)[0]
    dim = K.int_shape(z_mean)[1]
    # by default, random_normal has mean = 0 and std = 1.0
    epsilon = K.random_normal(shape=(batch, dim))
    return z_mean + K.exp(0.5 * z_log_var) * epsilon


# In[4]:


def plot_results(models,
                 data,
                 batch_size=500,#128,
                 model_name="vae_gsq"):
    """Plots labels and MNIST digits as a function of the 2D latent vector
    # Arguments
        models (tuple): encoder and decoder models
        data (tuple): test data and label
        batch_size (int): prediction batch size
        model_name (string): which model is using this function
    """

    encoder, decoder = models
    x_test, y_test = data
    os.makedirs(model_name, exist_ok=True)

    filename = os.path.join(model_name, "vae_mean.png")
    # display a 2D plot of the object classes in the latent space
    z_mean, _, _ = encoder.predict(x_test,
                                   batch_size=batch_size)
    plt.figure(figsize=(12, 10))
    plt.scatter(z_mean[:, 0], z_mean[:, 1], c=y_test)
    plt.colorbar()
    plt.title('Mapping Training Images to Latent Plane',fontsize=26)
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.savefig(filename)
    plt.show()

    filename = os.path.join(model_name, "latent.png")
    # display a nxn 2D manifold of digits
    n =5 
    digit_size = 28
    figure = np.zeros((digit_size * n, digit_size * n))
    # linearly spaced coordinates corresponding to the 2D plot
    # of digit classes in the latent space
    grid_x = np.linspace(-4, 4, n)
    grid_y = np.linspace(-4, 4, n)[::-1]

    for i, yi in enumerate(grid_y):
        for j, xi in enumerate(grid_x):
            z_sample = np.array([[xi, yi]])
            x_decoded = decoder.predict(z_sample)
            digit = x_decoded[0].reshape(digit_size, digit_size)
            figure[i * digit_size: (i + 1) * digit_size,
                   j * digit_size: (j + 1) * digit_size] = digit

    plt.figure(figsize=(10, 10))
    start_range = digit_size // 2
    end_range = (n - 1) * digit_size + start_range + 1
    pixel_range = np.arange(start_range, end_range, digit_size)
    sample_range_x = np.round(grid_x, 1)
    sample_range_y = np.round(grid_y, 1)
    plt.title('Image Generation from Latent Variable Data',fontsize=26)
    plt.xticks(pixel_range, sample_range_x)
    plt.yticks(pixel_range, sample_range_y)
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.imshow(figure)#, cmap='Greys_r')
    plt.savefig(filename)
    plt.show()


# network parameters
input_shape = (original_dim, )
intermediate_dim = 512
batch_size = 500#128
latent_dim = 2
epochs = 100#50


# In[7]:


# VAE model = encoder + decoder
# build encoder model
inputs = Input(shape=input_shape, name='encoder_input')
x = Dense(intermediate_dim, activation='relu')(inputs)
z_mean = Dense(latent_dim, name='z_mean')(x)
z_log_var = Dense(latent_dim, name='z_log_var')(x)


# In[8]:


# use reparameterization trick to push the sampling out as input
# note that "output_shape" isn't necessary with the TensorFlow backend
z = Lambda(sampling, output_shape=(latent_dim,), name='z')([z_mean, z_log_var])


# In[9]:


# instantiate encoder model
encoder = Model(inputs, [z_mean, z_log_var, z], name='encoder')
encoder.summary()
plot_model(encoder, to_file='vae_mlp_encoder.png', show_shapes=True)


# In[10]:


# build decoder model
latent_inputs = Input(shape=(latent_dim,), name='z_sampling')
x = Dense(intermediate_dim, activation='relu')(latent_inputs)
outputs = Dense(original_dim, activation='sigmoid')(x)


# In[11]:


# instantiate decoder model
decoder = Model(latent_inputs, outputs, name='decoder')
decoder.summary()
plot_model(decoder, to_file='vae_mlp_decoder.png', show_shapes=True)


# In[12]:


# instantiate VAE model
outputs = decoder(encoder(inputs)[2])
vae = Model(inputs, outputs, name='vae_mlp')


# In[14]:


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    help_ = "Load h5 model trained weights"
    parser.add_argument("-w", "--weights", help=help_)
    help_ = "Use mse loss instead of binary cross entropy (default)"
    parser.add_argument("-m",
                        "--mse",
                        help=help_, action='store_true')
    args = parser.parse_args()
    models = (encoder, decoder)
    data = (x_test, y_test)

    # VAE loss = mse_loss or xent_loss + kl_loss
    if args.mse:
        reconstruction_loss = mse(inputs, outputs)
    else:
        reconstruction_loss = binary_crossentropy(inputs,
                                                  outputs)

    reconstruction_loss *= original_dim
    kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
    kl_loss = K.sum(kl_loss, axis=-1)
    kl_loss *= -0.5
    vae_loss = K.mean(reconstruction_loss + kl_loss)
    vae.add_loss(vae_loss)
    vae.compile(optimizer='adam')
    vae.summary()
    plot_model(vae,
               to_file='vae_mlp.png',
               show_shapes=True)

    if args.weights:
        vae.load_weights(args.weights)
    else:
        # train the autoencoder
        vae.fit(x_train,
                epochs=epochs,
                batch_size=batch_size,
                validation_data=(x_test, None),
                callbacks=[TensorBoard(log_dir='/tmp/autoencoder_vae')])
        vae.save_weights('vae_mlp_gsq.h5')
    plot_results(models,
                 data,
                 batch_size=batch_size,
    model_name="vae_mlp")
